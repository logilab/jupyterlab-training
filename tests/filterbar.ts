import { Selector } from 'testcafe';
import { clean_jupyterlab_state } from './utils';

fixture`filterbar`.page`http://localhost:8888/?token=e2e-test`.beforeEach(
  async t => {
    await clean_jupyterlab_state(t);
  }
);

const searchbar_input = Selector('#filter input');
const searchbar_clearbutton = Selector('#filter .close');
const a_tag = Selector('a.badge');

test('clear', async t => {
  const some_text = 'this is some text';
  await t
    .expect(searchbar_input.value)
    .eql('')
    .typeText(searchbar_input, some_text)
    .expect(searchbar_input.value)
    .eql(some_text)
    .click(searchbar_clearbutton)
    .expect(searchbar_input.value)
    .eql('');
});

test('add_a_tag', async t => {
  await t
    .click(a_tag)
    .expect('1')
    .eql(await a_tag.innerText);
});
