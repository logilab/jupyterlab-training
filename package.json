{
    "name": "jupyterlab-training",
    "version": "0.5.0",
    "description": "jupyterlab training courses and exercices platform.",
    "keywords": [
        "jupyter",
        "jupyterlab",
        "jupyterlab-extension"
    ],
    "homepage": "https://gitlab.com/logilab/jupyterlab-training",
    "bugs": {
        "url": "https://gitlab.com/logilab/jupyterlab-training/issues"
    },
    "license": "BSD-3-Clause",
    "author": {
        "name": "Logilab",
        "email": "ogiorgis@logilab"
    },
    "files": [
        "lib/**/*.{d.ts,eot,gif,html,jpg,js,js.map,json,png,svg,woff2,ttf}",
        "style/**/*.{css,eot,gif,html,jpg,json,png,svg,woff2,ttf}",
        "style/index.js",
        "schema/*.json"
    ],
    "main": "lib/index.js",
    "types": "lib/index.d.ts",
    "style": "style/index.css",
    "repository": {
        "type": "git",
        "url": "https://gitlab.com/logilab/jupyterlab-training.git"
    },
    "scripts": {
        "build": "jlpm build:lib && jlpm build:labextension:dev",
        "build:prod": "jlpm clean && jlpm build:lib:prod && jlpm build:labextension",
        "build:labextension": "jupyter labextension build .",
        "build:labextension:dev": "jupyter labextension build --development True .",
        "build:lib": "tsc --sourceMap",
        "build:lib:prod": "tsc",
        "clean": "jlpm clean:lib",
        "clean:lib": "rimraf lib tsconfig.tsbuildinfo",
        "clean:lintcache": "rimraf .eslintcache .stylelintcache",
        "clean:labextension": "rimraf jupyterlab_training/labextension jupyterlab_training/_version.py",
        "clean:all": "jlpm clean:lib && jlpm clean:labextension && jlpm clean:lintcache",
        "eslint": "jlpm eslint:check --fix",
        "eslint:check": "eslint . --cache src",
        "install:extension": "jlpm build",
        "lint": "eslint src",
        "prettier": "jlpm prettier:base --write --list-different",
        "prettier:base": "prettier \"**/*{.ts,.tsx,.js,.jsx,.css,.json,.md}\"",
        "prettier:check": "jlpm prettier:base --check",
        "stylelint": "jlpm stylelint:check --fix",
        "stylelint:check": "stylelint --cache \"style/**/*.css\"",
        "watch": "run-p watch:src watch:labextension",
        "watch:src": "tsc -w",
        "watch:labextension": "jupyter labextension watch .",
        "test": "testcafe \"firefox:headless\" --skip-js-errors -S -s tests/screenshots/ tests/",
        "test-debug": "testcafe firefox --skip-js-errors tests/"
    },
    "dependencies": {
        "@eslint/eslintrc": "^3.2.0",
        "@eslint/js": "^9.20.0",
        "@jupyterlab/application": "^4.3.5",
        "@jupyterlab/apputils": "^4.4.5",
        "@jupyterlab/cells": "^4.3.5",
        "@jupyterlab/coreutils": "^6.3.5",
        "@jupyterlab/docmanager": "^4.3.5",
        "@jupyterlab/docregistry": "^4.3.5",
        "@jupyterlab/mainmenu": "^4.3.5",
        "@jupyterlab/notebook": "^4.3.5",
        "@jupyterlab/services": "^7.3.5",
        "@jupyterlab/ui-components": "^4.3.5",
        "@lumino/disposable": "^2.1.3",
        "@lumino/signaling": "^2.1.3",
        "@lumino/widgets": "^2.3.1",
        "@types/file-saver": "^2.0.7",
        "@types/js-yaml": "^4.0.9",
        "@types/luxon": "^3.4.2",
        "bootstrap": "^5.3.3",
        "file-saver": "^2.0.5",
        "i18next": "^24.2.2",
        "i18next-browser-languagedetector": "^8.0.4",
        "js-yaml": "^4.1.0",
        "jszip": "^3.10.1",
        "luxon": "^3.5.0",
        "react": "^18.0.26",
        "react-bootstrap": "2.10.0",
        "react-dom": "^18.0.9"
    },
    "devDependencies": {
        "@jupyterlab/builder": "^4.3.5",
        "@types/react": "^18.0.26",
        "@types/react-dom": "^18.0.9",
        "@typescript-eslint/eslint-plugin": "^8.24.1",
        "@typescript-eslint/parser": "^8.24.1",
        "css-loader": "^7.1.2",
        "eslint": "^9.20.1",
        "eslint-config-prettier": "^10.0.1",
        "eslint-plugin-prettier": "^5.2.3",
        "npm-run-all": "^4.1.5",
        "prettier": "^3.5.1",
        "rimraf": "^6.0.1",
        "stylelint": "^16.14.1",
        "testcafe": "^2.6.2",
        "typescript": "~5.7.3"
    },
    "sideEffects": [
        "style/*.css"
    ],
    "resolutions": {
        "@types/node": "^18.0.0",
        "@types/react": "^18.0.26",
        "react": "^18.2.0"
    },
    "jupyterlab": {
        "extension": true,
        "schemaDir": "schema",
        "outputDir": "jupyterlab_training/labextension"
    },
    "eslintIgnore": [
        "node_modules",
        "dist",
        "coverage",
        "**/*.d.ts",
        "tests",
        "**/__tests__",
        "ui-tests"
    ],
    "eslintConfig": {
        "extends": [
            "eslint:recommended",
            "plugin:@typescript-eslint/eslint-recommended",
            "plugin:@typescript-eslint/recommended",
            "plugin:prettier/recommended"
        ],
        "parser": "@typescript-eslint/parser",
        "parserOptions": {
            "project": "tsconfig.json",
            "sourceType": "module"
        },
        "plugins": [
            "@typescript-eslint"
        ],
        "rules": {
            "@typescript-eslint/naming-convention": [
                "error",
                {
                    "selector": "interface",
                    "format": [
                        "PascalCase"
                    ],
                    "custom": {
                        "regex": "^I[A-Z]",
                        "match": true
                    }
                }
            ],
            "@typescript-eslint/no-unused-vars": [
                "warn",
                {
                    "args": "none"
                }
            ],
            "@typescript-eslint/no-explicit-any": "off",
            "@typescript-eslint/no-namespace": "off",
            "@typescript-eslint/no-use-before-define": "off",
            "@typescript-eslint/quotes": [
                "error",
                "single",
                {
                    "avoidEscape": true,
                    "allowTemplateLiterals": false
                }
            ],
            "curly": [
                "error",
                "all"
            ],
            "eqeqeq": "error",
            "prefer-arrow-callback": "error"
        }
    },
    "prettier": {
        "singleQuote": true,
        "trailingComma": "none",
        "arrowParens": "avoid",
        "endOfLine": "auto",
        "overrides": [
            {
                "files": "package.json",
                "options": {
                    "tabWidth": 4
                }
            }
        ]
    },
    "stylelint": {
        "extends": [
            "stylelint-config-recommended",
            "stylelint-config-standard"
        ],
        "rules": {
            "property-no-vendor-prefix": null,
            "selector-class-pattern": null,
            "selector-no-vendor-prefix": null,
            "value-no-vendor-prefix": null
        }
    },
    "packageManager": "yarn@3.6.3"
}
