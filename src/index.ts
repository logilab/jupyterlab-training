import {
  ILabShell,
  ILayoutRestorer,
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';
import { IDocumentManager } from '@jupyterlab/docmanager';
import { INotebookTracker } from '@jupyterlab/notebook';
import { IMainMenu } from '@jupyterlab/mainmenu';
import { MainAreaWidget, ICommandPalette } from '@jupyterlab/apputils';
import { Menu } from '@lumino/widgets';
import { LabIcon } from '@jupyterlab/ui-components';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/index.css';

import { FormationTOC } from './menu';
import { ProgressButtons } from './progressButton';
import { StatsWidget } from './statsWidget';
import { addTour } from './tour';
import logilabIcon from '../style/logilab-icon.svg';

/**
 * Initialization data for the jupyterlab-formation Panel extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: 'jupyterlab-training',
  autoStart: true,
  requires: [
    IDocumentManager,
    ILabShell,
    ILayoutRestorer,
    INotebookTracker,
    ICommandPalette,
    IMainMenu
  ],
  activate: activate
};

/**
 * Activate the formation extension.
 */
function activate(
  app: JupyterFrontEnd,
  docmanager: IDocumentManager,
  labShell: ILabShell,
  restorer: ILayoutRestorer,
  notebookTracker: INotebookTracker,
  palette: ICommandPalette,
  mainMenu: IMainMenu
): void {
  console.log('JupyterLab extension jupyterlab-training-launcher');
  // Create the formation panel widget.
  const { commands, serviceManager } = app;
  const formationPanel = new FormationTOC({
    docmanager,
    notebookTracker,
    serviceManager
  });
  // Add the formation panel to the left area.
  const icon = new LabIcon({
    name: 'Training Menu',
    svgstr: logilabIcon
  });
  formationPanel.title.caption = 'Training Menu';
  formationPanel.title.icon = icon;
  formationPanel.title.iconClass = 'formation-icon lm-mod-current';
  formationPanel.id = 'tab-manager';
  app.shell.add(formationPanel, 'left', { rank: 10 });
  // Add the formation widget to the application restorer.
  restorer.add(formationPanel, formationPanel.id);
  // Change the LogilabPanel when the active widget changes.
  notebookTracker.currentChanged.connect(() => {
    formationPanel.currentWidget = notebookTracker.currentWidget;
    formationPanel.update();
  });

  // progress button
  function updateMenu(): void {
    formationPanel.updateMenu();
  }

  const progressButtons = new ProgressButtons();
  progressButtons.stateChanged.connect(updateMenu);
  app.docRegistry.addWidgetExtension('Notebook', progressButtons);

  // stats buttonExtension
  let command = 'training:stats';
  commands.addCommand(command, {
    label: 'Display Student Statistics',
    caption: 'Display students statistics',
    execute: () => {
      const content = new StatsWidget();
      const widget = new MainAreaWidget<StatsWidget>({ content });
      widget.title.label = 'Students statistics';
      app.shell.add(widget, 'main');
    }
  });
  palette.addItem({ command, category: 'Training' });
  const statsMenu = new Menu({ commands });
  statsMenu.title.label = 'Trainer';
  statsMenu.id = 'training';
  mainMenu.addMenu(statsMenu, false, { rank: 80 });
  statsMenu.addItem({ command });
  command = 'url-pdf';
  statsMenu.addItem({ command }); // for signature pdf extension
  console.log(
    'JupyterLab extension jupyterlab-training-launcher is activated!'
  );
  app.restored.then(() => {
    // open formation panel by default
    labShell.activateById(formationPanel.id);
    // add extension tour
    addTour(app, labShell, notebookTracker, docmanager, formationPanel.id);
  });
}

export default extension;
