import { Selector } from 'testcafe';
import { clean_jupyterlab_state } from './utils';

fixture`exercises`.page`http://localhost:8888/?token=e2e-test`.beforeEach(
  async t => {
    await clean_jupyterlab_state(t, 'exercises-tab');
  }
);

test('unittest_solution_hidden', async t => {
  const an_exercise = Selector('#exercises').find('.button-title');
  const code_cells = Selector('.jp-CodeMirrorEditor').filterVisible();
  const show_more_buttons = Selector('.toggleFormationCellButton');

  // check that only one code cell is visible
  await t
    .click(an_exercise)
    .expect(code_cells.count)
    .eql(1)
    .expect(show_more_buttons.count)
    .eql(2);

  await t.click(show_more_buttons); // make it appear
  await t.expect(code_cells.count).eql(2);
});
