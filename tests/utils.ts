import { Selector } from 'testcafe';

/*  Make sure:
    - we are on the extension panel
    - on courses tab
    - there is not opened exercise
    - single document is disabled
    - plugin language is english
*/
export async function clean_jupyterlab_state(
  t: any,
  wanted_tab = 'exercises-tab'
) {
  const tab_bar = Selector('.lm-DockPanel-tabBar');
  if (!(await tab_bar.visible)) {
    // Open view menu
    await t.click(Selector('.lm-MenuBar-itemLabel').withText('View'));
    // disable Single document disable
    await t.click(
      Selector('li.lm-Menu-item').withAttribute(
        'data-command',
        'application:toggle-mode'
      )
    );
  }
  const current_panel_button = Selector('.lm-TabBar-tab.lm-mod-current');
  const training_panel_button = Selector('.lm-TabBar-tab').withAttribute(
    'title',
    ''
  );
  const training_courses_tab_button = Selector('.nav-link').withAttribute(
    'id',
    wanted_tab
  );
  const toggle_options = Selector('#toggle-options');
  const language_button = Selector('#lang');
  const language_option = language_button.find('option');

  if (
    (await current_panel_button.getAttribute('title')) !==
    (await training_panel_button.getAttribute('title'))
  ) {
    await t.click(training_panel_button);
  }
  // toggle advanced options
  await t.wait(5).click(toggle_options);
  await t.click(language_button).click(language_option.withText('en'));
  await t.click(toggle_options);
  await t.click(training_courses_tab_button);
}
